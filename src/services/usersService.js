const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserByUserId = async (userId) => {
  const user = await User.findOne({_id: userId}, '-__v -password');
  return user;
};

const changePasswordByIdForUser = async (userId, oldPassword, newPassword) => {
  const user = await User.findOne({_id: userId});

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Invalid password');
  }
  const newPass = await bcrypt.hash(newPassword, 10);
  await User.findOneAndUpdate({_id: userId}, {$set: {password: newPass}});
};

const deleteProfileByIdForUser = async (userId) => {
  await User.findOneAndRemove({_id: userId});
};

module.exports = {
  getUserByUserId,
  deleteProfileByIdForUser,
  changePasswordByIdForUser,
};
