const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId, offset = '0', limit = '0') => {
  const notes = await Note.find({userId}, '-__v')
      .skip(parseInt(offset, 10))
      .limit(parseInt(limit, 10));
  return notes;
};

const getNoteByIdForUser = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId}, '-__v');
  return note;
};

const addNoteToUser = async (userId, notePayload) => {
  if (!!notePayload.completed !== true) {
    notePayload.completed = false;
  }
  const note = new Note({...notePayload, userId});
  await note.save();
};

const updateNoteByIdForUser = async (userId, noteId, data) => {
  await Note.findOneAndUpdate({_id: noteId, userId}, {$set: data});
};

const checkNoteByIdForUser = async (userId, noteId) => {
  const user = await Note.findOne({_id: noteId, userId});
  await Note.findOneAndUpdate({_id: noteId, userId},
      {$set: {completed: !user.completed}});
};

const deleteNoteByIdForUser = async (noteId, userId) => {
  await Note.findOneAndRemove({_id: noteId, userId});
};

module.exports = {
  getNotesByUserId,
  getNoteByIdForUser,
  addNoteToUser,
  updateNoteByIdForUser,
  checkNoteByIdForUser,
  deleteNoteByIdForUser,
};
