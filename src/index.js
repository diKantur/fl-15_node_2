const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
require('dotenv').config();

const {notesRouter} = require('./controllers/notesController');
const {usersRouter} = require('./controllers/usersController');
const {authRouter} = require('./controllers/authController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {NodeCourseError} = require('./utils/errors');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/notes', [authMiddleware], notesRouter);
app.use('/api/users', [authMiddleware], usersRouter);

app.use((err, req, res, next) => {
  res.status(400).json({message: err.message});
});

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://testuser24:testuser24@cluster0.bbgt1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
      useNewUrlParser: true, useUnifiedTopology: true,
    });

    app.listen(process.env.PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
