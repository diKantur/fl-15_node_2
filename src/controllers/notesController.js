const express = require('express');
const router = express.Router();

const {getNotesByUserId,
  getNoteByIdForUser,
  addNoteToUser,
  updateNoteByIdForUser,
  checkNoteByIdForUser,
  deleteNoteByIdForUser} = require('../services/notesService');

const {asyncWrapper} = require('../utils/apiUtils');
const {InvalidRequestError} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {offset, limit} = req.query;

  const result = {
    offset: offset,
    limit: limit,
    count: 0,
    notes: await getNotesByUserId(userId, offset, limit),
  };

  res.json(result);
}),
);

router.post('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await addNoteToUser(userId, req.body);

  res.json({message: 'Success'});
}),
);

router.get('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  const note = await getNoteByIdForUser(id, userId);

  if (!note) {
    throw new InvalidRequestError('No note with such id found!');
  }

  res.json({note});
}),
);

router.put('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const data = req.body;

  await updateNoteByIdForUser(userId, id, data);

  res.json({message: 'Success'});
}),
);

router.patch('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await checkNoteByIdForUser(userId, id);

  res.json({message: 'Success'});
}),
);


router.delete('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await deleteNoteByIdForUser(id, userId);

  res.json({message: 'Success'});
}),
);

module.exports = {
  notesRouter: router,
};
