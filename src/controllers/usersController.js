const express = require('express');
const router = express.Router();

const {
  getUserByUserId,
  deleteProfileByIdForUser,
  changePasswordByIdForUser,
} = require('../services/usersService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const user = await getUserByUserId(userId);

  res.json({user});
}));

router.delete('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await deleteProfileByIdForUser(userId);

  res.json({message: 'Success'});
}));

router.patch('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {oldPassword, newPassword} = req.body;

  await changePasswordByIdForUser(userId, oldPassword, newPassword);

  res.json({message: 'Success'});
}));

module.exports = {
  usersRouter: router,
};
